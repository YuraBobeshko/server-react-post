require("dotenv").config();

module.exports = {
  development: {
    username: process.env.DB_USER,
    password: process.env.DB_PASS,
    database: process.env.DB_NAME,
    host: process.env.DB_HOST,
    dialect: "mysql",
    operatorsAliases: 0,
    seederStorage: "sequelize",
    logging: false,
    define: {
      timestamps: false,
    },
  },
  // test: {
  //   username: process.env.DB_USER,
  //   password: process.env.DB_PASS,
  //   database: process.env.DB_NAME,
  //   host: process.env.DB_HOST,
  //   dialect: "mysql",
  //   operatorsAliases: 0,
  //   seederStorage: "sequelize",
  //   logging: false,
  //   define: {
  //     timestamps: false,
  //   },
  // },
  // production: {
  //   username: process.env.DB_USER,
  //   password: process.env.DB_PASS,
  //   database: process.env.DB_NAME,
  //   host: process.env.DB_HOST,
  //   dialect: "mysql",
  //   operatorsAliases: 0,
  //   seederStorage: "sequelize",
  //   logging: false,
  //   define: {
  //     timestamps: false,
  //   },
  // },
};
