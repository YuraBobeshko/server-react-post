"use strict";

// import fs from "fs";
// import path from "path";
// import Sequelize from "sequelize";

const fs = require("fs");
const path = require("path");
const Sequelize = require("sequelize");

const basename = path.basename(__filename);
const config = require(`${__dirname}/../config/db`);
const models = {};

const sequelize = new Sequelize(config.development);

fs.readdirSync(__dirname)
  .filter(
    (file) =>
      file.indexOf(".") !== 0 && file !== basename && file.slice(-3) === ".js"
  )
  .forEach((file) => {
    const model = require(path.join(__dirname, file));
    console.log(model);

    models[model.name] = model.init(sequelize, Sequelize);
  });

Object.values(models)
  .filter((model) => typeof model.associate === "function")
  .forEach((model) => model.associate(models));

module.exports = {
  ...models,
  sequelize,
  Sequelize,
};

// const Sequelize = require("sequelize");
// const configDB = require("../config/db");

// sequelize = new Sequelize(configDB.development);

// const Students = require("./Students")(sequelize);
// const Books = require("./Books")(sequelize);

// // test DB
// sequelize
//   .authenticate()
//   .then(() => console.log("db connected"))
//   .catch((err) => console.log("db connected error: " + err));

// Students.sync().then(() => {
//   console.log("Students created");
// });

// Books.sync().then(() => {
//   console.log("Books created");
// });

// module.exports = {
//   sequelize,
//   Students,
//   Books,
// };
