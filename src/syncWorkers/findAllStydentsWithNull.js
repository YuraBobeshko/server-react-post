const { Students } = require("../models/index");
const fs = require("fs");


fs.open("src/log/logs.txt", "w", (err) => {
  if (err) throw err;
});

module.exports = function findAllStydentsWithNull() {

  Students.findAll({
    where: {
      classId: null,
    },
  }).then((Students) =>
    fs.appendFile(
      "src/log/logs.txt",
      JSON.stringify(Students) + "\n",

      (err) => {
        if (err) throw err;
        console.log("Data has been added!");
      }
    )
  );

  setTimeout(findAllStydentsWithNull, 60000);
};
