"use strict";

module.exports = {
  up: async (queryInterface, Sequelize) =>
    await queryInterface.bulkInsert(
      "Students",
      [
        {
          firstName: "Джинни",
          lastName: "Уизли",
          classId: 4,
        },
        {
          firstName: "Том",
          lastName: "Реддл",
          classId: 4,
        },
        {
          firstName: "Гермио́на",
          lastName: "Гре́йнджер",
          classId: 2,
        },
        {
          firstName: "Рон",
          lastName: "Уизли",
          classId: 2,
        },
        {
          firstName: "John Doe",
          lastName: "John Doe",
          classId: null,
        },
        {
          firstName: "Га́рри",
          lastName: "По́ттер",
          classId: 2,
        },
        {
          firstName: "Не́вилл",
          lastName: "Долгопу́пс",
          classId: 2,
        },
        {
          firstName: "Драко",
          lastName: "Малфой",
          classId: 3,
        },
        {
          firstName: "Полу́мна",
          lastName: "Ла́вгуд",
          classId: 3,
        },
        {
          firstName: "Фред",
          lastName: "Уизли",
          classId: 1,
        },
        {
          firstName: "Джордж",
          lastName: "Уизли",
          classId: 1,
        },
        {
          firstName: "John Doe",
          lastName: "John Doe",
          classId: null,
        },
        {
          firstName: "John Doe",
          lastName: "John Doe",
          classId: null,
        },
      ],
      {}
    ),
  down: async (queryInterface, Sequelize) => {},
};
