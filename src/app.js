require("dotenv").config();
const express = require("express");
const app = express();
const port = process.env.PORT || 5000;
const bodyParser = require("body-parser");
const { Students } = require("./models/index");
app.use((req, res, next) => {
  res.set("Access-Control-Allow-Origin", "*");
  res.set("Access-Control-Allow-Headers", "content-type");
  res.set("Access-Control-Allow-Methods", "DELETE, PATCH");
  next();
});

app.post("/api/student/create", bodyParser.json(), (req, res) => {
  if (!req.body.firstName) throw "firstName is undefined";
  if (!req.body.lastName) throw "lastName is undefined";

  Students.create({
    firstName: req.body.firstName,
    lastName: req.body.lastName,
    classId: req.body.classId || null,
  })
    .then(() => res.json(Students.findAll().length))
    .catch((err) => console.log("error: " + err));
});

app.get("/api/students", (req, res) => {
  Students.findAll()
    .then((Students) => res.json(Students))
    .catch((err) => console.log("error: " + err));
});

require("./syncWorkers/findAllStydentsWithNull")();

app.listen(port, () => {
  console.log(`start on ${port}`);
});
